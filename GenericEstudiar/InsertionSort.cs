﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericEstudiar
{
    class InsertionSort
    {
        public static void Sort(float[] max)
        {
            for (int i = 1; i < max.Length; i++)
            {
                int j = i;
                while (j > 0)
                {
                    if (max[j - 1] > max[j])
                    {
                        float temp = max[j - 1];
                        max[j - 1] = max[j];
                        max[j] = temp;
                        j--;
                    }
                    else
                        break;
                }
            }
        }
    }
}
