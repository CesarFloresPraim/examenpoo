﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericEstudiar
{
    class Program
    {
        static void Main(string[] args)
        {
            //float[] max = { 15, 4.3f, 20, 10.12f, 3, 6, 1 };
            // int[] max = { 15, 4, 20, 10, 3, 6, 1 };
            Rectanguares[] max = { new Rectanguares(1, 5), new Rectanguares(4, 4), new Rectanguares(2, 2) };
            InsertionSortG.Sort(max);
            Console.Write("\n\nThe numbers in ascending orders are given below:\n\n");
            for (int i = 0; i < max.Length; i++)
            {
                Console.Write("Sorted [" + (i + 1).ToString() + "] element: ");
                Console.Write(max[i]);
                Console.Write("\n");
            }
            
        }
        class Rectanguares : IComparable
        {
            int x;
            int y;
            public Rectanguares(int a, int b)
            {
                x = a;
                y = b;
            }

            public int CompareTo(object obj)
            {
                if (((Rectanguares)obj).x + ((Rectanguares)obj).y > (x + y))
                {
                    return -1;
                }
                else if (((Rectanguares)obj).x + ((Rectanguares)obj).y == (x + y))
                {
                    return 0;
                }
                else return 1;
            }
            public override string ToString()
            {
                return "x: " + x + "y: " + y;
            }
        }
    }
}

