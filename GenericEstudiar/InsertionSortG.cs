﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericEstudiar
{
    class InsertionSortG
    {
        public static void Sort<T>(T[] max) where T : IComparable
        {
            for (int i = 1; i < max.Length; i++)
            {
                int j = i;
                while (j > 0)
                {
                    if ((max[j - 1].CompareTo(max[j]))>0)
                    {
                        T temp = max[j - 1];
                        max[j - 1] = max[j];
                        max[j] = temp;
                        j--;
                    }
                    else
                        break;
                }
            }
        }
    }
}
